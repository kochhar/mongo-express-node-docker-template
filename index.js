var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var session = require('express-session');
var http = require('http');
var mongoose = require('mongoose');
var morgan = require('morgan');

// application config
var port = process.env.DNM_PORT || 3000;

//DB setup
mongoose.connect("mongodb://mongo:27017");

var app = express();
// Log requests to the console output
app.use(morgan('combined'));
// parse `req.headers.cookie` (raw string) into `req.cookies` (keys -> values)
app.use(cookieParser());
// parse `application/x-www-form-urlencoded` form data into `req.body` (keys -> values)
app.use(bodyParser.urlencoded({ extended: false, type: 'application/x-www-form-urlencoded' }));
// parse `application/json` data into `req.json` (keys -> values)
app.use(bodyParser.json({ limit: '1mb' }));
// create user sessions
app.use(session({ secret: 'this is the rhythm of the night',
                  resave: false,
                  saveUninitialized: false,
                }));
// serve static assets from the 'www' folder
app.use(express.static('www'));

app.get('/:message', function(req, res) {
  res.send('Hey you sent me <b>'+req.params.message+'</b>');
});
app.listen(port);

console.log('Server running at http://127.0.0.1:'+port);
