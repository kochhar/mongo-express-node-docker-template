# README #

Steps needed to use this repository

### What is this repository for? ###

* Quick summary
Template for an isomorphic application which runs express and node in the backend and jquery+handlebars in the frontend.

* Version
1.0.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
To use this template to setup your application follow:

```
$ git init
$ git remote add template git@bitbucket.org:kochhar/mongo-express-node-docker-template.git
$ git pull template/master
$ git remote remote template
$ git push origin/master
```

* Configuration
* Dependencies
* Database configuration

* How to run tests
```$ npm test```

* Deployment instructions
```$ docker-compose up```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Questions? Comments? Drop me a message
