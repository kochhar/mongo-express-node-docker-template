'use strict';

import gulp from 'gulp';
import concat from 'gulp-concat';
import jshint from 'gulp-jshint';
import minifyCSS from 'gulp-minify-css';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';


// minify and concat css
gulp.task('css', () => {
  return gulp.src([
                  'node_modules/bootstrap/dist/css/bootstrap.css',
                  'node_modules/alpaca/dist/alpaca/bootstrap/alpaca.css',
                  'src/css/**/*.css'
                  ])
                  .pipe(minifyCSS())
                  .pipe(concat('style.css'))
                  .pipe(gulp.dest('www/css'));
});


// link task
gulp.task('app:lint', () => {
  return gulp.src('app/**/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
});


// minify, obfuscate and concat dependency js files
gulp.task('deps:js', () => {
  return gulp.src([
                  'node_modules/jquery/dist/jquery.js',
                  'node_modules/bootstrap/dist/js/bootstrap.js',
                  'node_modules/handlebars/dist/handlebars.js',
                  'node_modules/alpaca/dist/alpaca/bootstrap/alpaca.js',
                  ])
                  .pipe(sourcemaps.init())
                  .pipe(concat('deps.js'))
                  .pipe(gulp.dest('www/js'))        // save concatenated as .js
                  .pipe(uglify())
                  .pipe(rename({ extname: '.min.js' }))
                  .pipe(sourcemaps.write('maps'))   // materialise minified source maps
                  .pipe(gulp.dest('www/js'));       // save minified as .min.js
});


// minify, obfuscate and concat app js files
gulp.task('app:js', () => {
  return gulp.src([
                  'app/**/*.js'
                  ])
                  .pipe(sourcemaps.init())
                  .pipe(concat('app.js'))
                  .pipe(gulp.dest('www/js'))        // save concatenated as .js
                  .pipe(uglify())
                  .pipe(rename({ extname: '.min.js' }))
                  .pipe(sourcemaps.write('maps'))   // materialise minified source maps
                  .pipe(gulp.dest('www/js'));       // save minified as .min.js
});


// run all js
gulp.task('js', () => {
  gulp.run('deps:js', 'app:js');
});


// default run all
gulp.task('default', () => {
  gulp.run('app:lint', 'js', 'css');
});
