FROM node:8.5-alpine

RUN apk update
RUN apk add bash
RUN apk add less
RUN apk add htop
RUN npm install -g gulp-cli

# Create the app directory
RUN mkdir /opt/app
# Make it the working directory
WORKDIR /opt/app
# Copy the package dependencies and the version lock file
ADD package.json package-lock.json /opt/app/

# Install the dependencies
RUN npm install

# Expose port 3000
EXPOSE 3000
# and start the server
CMD ["npm", "start"]
